TRANSFLAGIFY
============

A bookmarklet which works around browsers not being able to show the trans flag
emoji: <https://emojipedia.org/transgender-flag/>

FAQ
---

### Which browsers are supported?

I've personally tested it with Firefox and Chrome, but any browser should work,
so long as it supports bookmarklets:

https://support.mozilla.org/en-US/kb/bookmarklets-perform-common-web-page-tasks

### Could this be modified to handle other "unsupported" emoji?

Yes - it simply replaces all instances of a given bit of text with an image tag
instead. Feel free to use this as the basis for something entirely unrelated.

### Why is this a bookmarklet, rather than a "real" browser extension?

Bookmarklets aren't tied to any specific browser, whereas add-ons are generally
added via the Chrome web store, addons.mozilla.org, etc.

### But a browser add-on could run automatically, without clicking a button!

This is true, but to work properly it would need to run not just when a page is
first loaded, but whenever it is changed dynamically via Javascript, and that's
a lot of hassle. Plus, it might slow things down by running when it didn't need
to...

Again, though, feel free to use this code as the basis for your own extensions.

### How do I beat beaver bother?

Heck if I know.

License
-------

Copyright 2019 Thomas Glyn Dennis

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
