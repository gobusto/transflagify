/*
TRANSFLAGIFY
============

Copyright 2019 Thomas Glyn Dennis

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

(function () {
  "use strict";

  // Get the text size of an element in pixels, even if the value is in `em` or
  // something. Note: I'm not sure if `.lineHeight` might be a better option...
  function textSizeOf(elem) {
    return parseInt(window.getComputedStyle(elem).fontSize, 10)
  }

  // Use XPATH to find all `#text` nodes on the page containing a given phrase.
  // For more details, see <https://developer.mozilla.org/en-US/docs/Web/XPath>
  function textContaining(phrase) {
    return document.evaluate(
      "//text()[contains(., '" + phrase + "')]",
      document,
      null,
      XPathResult.ORDERED_NODE_SNAPSHOT_TYPE,
      null
    )
  }

  // This is where the magic happens:
  function replaceTextWithImage(phrase, url) {
    var result = textContaining(phrase)
    for (var i = 0; i < result.snapshotLength; ++i) {
      var originalNode = result.snapshotItem(i)
      var containerNode = originalNode.parentNode
      var height = textSizeOf(containerNode) + "px"

      // Split the original text into pieces:
      originalNode.textContent.split(phrase).forEach(function (text, i) {
        // The first bit of text comes BEFORE the first flag, so skip that one:
        if (i > 0) {
          // Create an image with the same dimensions as the surrounding text:
          var img = document.createElement("img")
          img.src = url
          img.alt = phrase
          img.style.display = 'inline-block'
          img.style.height = height
          containerNode.insertBefore(img, originalNode)
        }

        // This is the bit of text we had originally:
        containerNode.insertBefore(document.createTextNode(text), originalNode)
      })

      // Get rid of the original text node:
      containerNode.removeChild(originalNode)
    }
  }

  // According to <https://emojipedia.org/transgender-flag>, the sequence for a
  // trans flag is `1F3F3 FE0F 200D 26A7`, so that's what I've used here... but
  // I've noticed that some browsers allow the `FE0F` or `200D` to be left out,
  // or even switched around, for similar "combining" emoji, so you may want to
  // copy/paste this line a few times in order to account for these variations:
  var flagImage = "https://emojipedia-us.s3.dualstack.us-west-1.amazonaws.com/thumbs/120/facebook/200/transgender-pride-flag_1f3f3-fe0f-200d-26a7.png"
  replaceTextWithImage("🏳\uFE0F\u200D⚧", flagImage)
})();
